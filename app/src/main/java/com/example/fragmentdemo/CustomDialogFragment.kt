package com.example.fragmentdemo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment

class CustomDialogFragment : DialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_custom_dialog, container, false)

        val cancelButton = view.findViewById<Button>(R.id.btnCancel)
        val submitButton = view.findViewById<Button>(R.id.btnOk)
        val radioGroup = view.findViewById<RadioGroup>(R.id.languageRadioGroup)

       cancelButton.setOnClickListener {
           dismiss()
       }

        submitButton.setOnClickListener {
            val selectedId = radioGroup.checkedRadioButtonId
            val selectedRadioButtonId = view.findViewById<RadioButton>(selectedId)

            if(selectedRadioButtonId!=null) {
                Toast.makeText(
                    context,
                    "Language Selected : ${selectedRadioButtonId.text}",
                    Toast.LENGTH_SHORT
                ).show()
            }else{
                Toast.makeText(
                    context,
                    "Nothing Selected",
                    Toast.LENGTH_SHORT
                ).show()
            }

            dismiss()
        }

        return view
    }

}